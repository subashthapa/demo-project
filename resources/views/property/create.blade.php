@extends('layouts.default')

@section('content')
	<div class="row">
		<div class="col-sm-6">
			@include ( 'errors.list' )
			{!! Form::open( array ('method' => 'post', 'route' => 'property.store', 'class' => 'form-horizontal', 'files' => 'true')) !!}
				<div class="form-group">
		       			{!! Form::label('address', 'Address:', ['class'=>'col-sm-2']) !!}
		       			<div class="col-sm-10">
		       				{!! Form::text('address', null, ['class'=>'form-control']) !!}
		       			</div>
	       			</div><!-- end of form-group -->

	       			<div class="form-group">
		       			{!! Form::label('type', 'Type:', ['class'=>'col-sm-2']) !!}
		       			<div class="col-sm-10">
		       				{!! Form::text('type', null, ['class'=>'form-control']) !!}
		       			</div>
	       			</div><!-- end of form-group -->

	       			<div class="form-group">
		       			{!! Form::label('images', 'Image:', ['class'=>'col-sm-2']) !!}
		       			<div class="col-sm-10">
		       				{!! Form::file('images[]', ['class'=>'form-control', 'multiple']) !!}
		       			</div>
	       			</div><!-- end of form-group -->

	       			<div class="form-group">
		       			{!! Form::label('submit', '', ['class'=>'col-sm-2']) !!}
		       			<div class="col-sm-4">
		       				{!! Form::submit('Add property', ['class'=>'btn btn-primary form-control']) !!}
		       			</div>
	       			</div><!-- end of form-group -->
			{!! Form::close() !!}
		</div><!-- end of col-sm-6 -->
	</div><!-- end of row -->

@endsection