@extends('layouts.default')

@section('content')
	@foreach($properties as $prop)
		<div class="row"><p>
			<div class="col-sm-3">
				<b>{{ $prop->address }}</b><br/>
				<p>{{ $prop->type }}</p>
			</div><!-- /col-sm-3 -->
			<div class="col-sm-3">
				<a href="{{ Url('/property') }}/{{ $prop->id }}">
					<button class="btn btn-primary"><i class="glyphicon glyphicon-heart"></i> View more</button>
				</a>
			</div><!-- /col-sm-3 -->
		</p>
		</div><!-- /row -->
	@endforeach
@endsection