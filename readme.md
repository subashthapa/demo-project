# Demo Project - Subash

This project allows user to upload multiple images. Users can go through images after uploading.



## URLs

1. /property
2. /property/create


## Database

Migration files are available. Database credentials are inside .env file in the root directory of the project.


## Validation

Users are not validated and edit & update features are not available at this moment as well.
Images are validated.


## Requirements not met
1. Connections over SSL
2. Complete implementation of threejs


## Libraries

1. intervention/image -- https://github.com/Intervention/image
   - to manipulate images
   - makes easy to work with images

2. laracasts/flash -- https://github.com/laracasts/flash
   - flash messages

3. laravelcollective/html -- https://github.com/LaravelCollective/html
   - enables the use of codes like 'Html' & 'Form' to build form and work with HTML.

4. Blade template -- 
   - template engine. All files must be named like 'example.blade.php' instead of 'example.php'


#Happy side
Use of three.js library


## Plan for next phase
1. Break down PropertyController
2. Work on 3D carousal