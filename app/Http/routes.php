<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('property', 'PropertyController@index');
Route::get('property/create', 'PropertyController@create');
Route::post('property', ['as' => 'property.store', 'uses' => 'PropertyController@store']);
Route::get('property/{id}', 'PropertyController@show');
