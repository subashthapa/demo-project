<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Input;
use Flash;
use App\Property;
use App\Propertyimage;
use App\Http\Requests;
use App\Http\Requests\PropertyRequest;

class PropertyController extends Controller
{
    /**
    * List all the properties
    *
    * @properties
    */
    public function index() 
    {
    	$properties = Property::with('propertyimage')->get();
    	return view('property.index', compact('properties'));
    }

    /**
    * Display a form to add a property
    *
    * @create
    */
    public function create() 
    {
    	return view('property.create');
    }

    /**
    * Store property & images
    *
    * @store
    */
    public function store(PropertyRequest $request)
    {
        /**
        * Get data using form and save to database
        */
		$property = new Property;
		$property->address = $request->input('address');
		$property->type = $request->input('type');
		$property->save();

        /**
        * Get the id of inserted property
        */
		$id = $property->id;

        /**
        * Get images & upload to different table
        */
        $clientfiles = Input::file('images');

        if(!empty($clientfiles)){

    		foreach($clientfiles as $cf)
            {
                $filename  = 'property_thumb_' . time() . '.' . $cf->getClientOriginalExtension();
                $originalFilename  = 'property_' . time() . '.' . $cf->getClientOriginalExtension();

                $path = public_path('images/thumbnails/' . $filename);
                $originalPath = public_path('images/' . $originalFilename);
                
                /**
                * Check if filename already exists in directory
                */
                if(file_exists($path))
                {
                    $filename = 'property_thumb_1'. time(). '.'.$cf->getClientOriginalExtension();
                    $path = public_path('images/thumbnails/' . $filename);
                }

                /**
                * Check if filename already exists in directory
                */
                if(file_exists($originalPath))
                {
                    $originalFilename = 'property_1'. time(). '.'.$cf->getClientOriginalExtension();
                    $originalPath = public_path('images/' . $originalFilename);
                }

                /**
                * Use of Image library to save image and create a thumbnail
                */
                Image::make($cf->getRealPath())->save($originalPath);
                Image::make($originalPath)->resize(512, 512)->save($path);
                
                /**
                * Save the image name in database
                */
                $propimage = new Propertyimage;
                $propimage->thumbnail = $filename;
                $propimage->imagename = $originalFilename;
                $propimage->property_id = $id;
                $propimage->save();
            } // end foreach(...)

            flash::success('Your property has been added');
        } // end if(...)

        return redirect('/property');

    }

    /**
    * Display the selected property
    *
    * @property
    */
    public function show($id) 
    {
        $property = Property::findOrFail($id);
        return view('property.show', compact('property'));
    }


    public function edit($id) {}
    public function update($id) {}
    public function delete($id) {}
}
