<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propertyimage extends Model
{
    protected $table = 'propertyimages';

    protected $fillable = [
    	'property_id',
    	'imagename',
    	'thumbnail'
    	];

    public function property()
	{
		return $this->belongsTo('App\Property');
	}
}