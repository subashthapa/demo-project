<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';

    protected $fillable = [
    	'address',
    	'type'
    	];


    public function Propertyimage() 
    {
    	return $this->hasMany('App\Propertyimage', 'property_id', 'id');
    }
}
